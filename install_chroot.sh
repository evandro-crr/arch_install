#!/bin/zsh

hostmane="microverse"
user="stark"

usermod -s /usr/bin/zsh root

ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

hwclock --systohc

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(pt_BR\.*\)/\1/' /etc/locale.gen
locale-gen

echo "LANG=pt_BR.UTF-8" > /etc/locale.conf
echo "KEYMAP=br-abnt2" > /etc/vconsole.conf
echo ${hostmane} > /etc/hostname

mkinitcpio -p linux

useradd -m -s /usr/bin/zsh -g wheel -G{uucp,lock} ${user}
mv setup.sh /home/${user}/

pacman -Syu wget i3 slim xorg xorg dhcpcd xcompmgr redshift feh xorg-xinit xorg-xbacklight python pavucontrol pulseaudio git nm-connection-editor network-manager-applet networkmanager dmenu --noconfirm

systemctl enable slim
systemctl enable dhcpcd
systemctl enable NetworkManager

sed -i "s/#\(default_user \).\+/\1${user}/" /etc/slim.conf

sed -i "s/# \(ALL ALL=(ALL) \).\+/\1NOPASSWD: ALL/" /etc/sudoers 
su -c "/home/${user}/setup.sh" -l ${user}
sed -i "s/\(ALL ALL=(ALL).\+\)/\# \1/" /etc/sudoers

sed -i "s/# \(%wheel ALL=(ALL) ALL\).\+/\1/" /etc/sudoers
