#!/bin/zsh

cd ~/; wget https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz
cd ~/; wget https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz

tar xvf ~/package-query.tar.gz
tar xvf ~/yaourt.tar.gz
rm ~/*.tar.gz

cd ~/package-query/; makepkg; sudo pacman -U *.pkg.tar.xz --noconfirm
rm -r ~/package-query/

cd ~/yaourt; makepkg; sudo pacman -U *.pkg.tar.xz --noconfirm
rm -r ~/yaourt/

yaourt -S google-chrome --noconfirm
yaourt -S ttf-dejavu-sans-mono-powerline-git --noconfirm
yaourt -S ttf-roboto --noconfirm
