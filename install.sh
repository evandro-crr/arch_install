#!/bin/zsh
boot=/dev/sdb1
root=/dev/sdb3
swap=/dev/sdb2
home=/dev/sdb4

mnt_root=/mnt
mnt_boot=/mnt/boot
mnt_home=/mnt/home

mkfs.fat -F32 ${boot}
mkfs.ext4 ${root}
mkfs.ext4 ${home}
mkswap ${swap}

mount ${root} ${mnt_root}
mkdir ${mnt_boot} ${mnt_home}
mount ${home} ${mnt_home}
mout  ${boot} ${mnt_boot}

timedatectl set-ntp true

pacstrap ${mnt_root} base base-devel zsh
genfstab -U ${mnt_root} >> ${mnt_root}/etc/fstab

cp packages.both ${mnt_root}
cp setup.sh ${mnt_root}
cp install_chroot.sh ${mnt_root}

cp /etc/zsh/zshrc ${mnt_root}/etc/zsh/zshrc 
